using GameBackend.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;

namespace GameBackend.Services
{
    public class TokensReqUpdateResult
    {
        public int TokensReq { get; set; }
        public string TxnHash { get; set; }
        public string Time { get; set; }
        public bool HashExists { get; set; }
    }

    public class TokenRequestService : ITokenRequestService
    {
        private readonly IMongoDatabase _database;

        public TokenRequestService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Player> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Player>(collectionName);
        }

        public async Task<TokensReqUpdateResult> UpdateTokenReq(string collectionName, string userId, string? hash = null)
        {

            var collection = GetCollectionByName(collectionName);

            // Retrieve the player document by the provided userId
            var player = await collection.Find(x => x.UserId == userId).FirstOrDefaultAsync();

            if (player == null)
            {
                throw new ArgumentException("Player not found.");
            }

            if (!string.IsNullOrEmpty(player.TokensReqData.Txnhash) && player.TokensReqData.Txnhash != "empty")
            {

                var resultWithExistingHash = new TokensReqUpdateResult
                {
                    HashExists = true
                };
                return resultWithExistingHash;
            }

            // Retrieve the current total score from the player object
            var totalScore = player.ScoreData.TotalScore;
            var now = DateTime.UtcNow;
            var time = now.ToString("yyyy-MM-dd HH:mm:ss");

            var update = Builders<Player>.Update
                .Set(x => x.ScoreData.TotalScore, 0)
                .Set(x => x.ScoreData.TSUpdated, now)
                .Set(x => x.TokensReqData.TokensReqValue, totalScore)
                .Set(x => x.TokensReqData.Txnhash, hash)
                .Set(x => x.TokensReqData.TRUpdated, now);

            await collection.UpdateOneAsync(x => x.UserId == userId, update);

            var tokensReq = player.TokensReqData.TokensReqValue;
            var txnHash = player.TokensReqData.Txnhash;

            var result = new TokensReqUpdateResult
            {
                TokensReq = totalScore,
                TxnHash = hash,
                Time = time,
                HashExists = false
            };

            return result;
        }
    }

}