using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IAutosignerTokenService
    {
        Task CreateNewNode(string collectionName, AutosignerToken newNode);
        Task UpdateBearerToken(string collectionName, string node, AutosignerToken newNode);
        Task<AutosignerToken?> GetBearerToken(string collectionName, string node);
    }

}
