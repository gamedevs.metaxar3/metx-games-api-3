using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface ITokenClaimService
    {
        Task<Player?> GetPlayerByUserId(string collectionName, string userId);
        int GetTokensRequestedAmount(TokensReq? tokensReqRef);
        int GetTokensClaimed(TokensClaim? tokensClaimRef);
        int GetTotalScore(Score scoreRef);
        Task<ClaimStatusResult> CheckClaimStatus(string collectionName, string userId, string? autosigner_url = null);
        Task<ClaimStatusResult> UpdatePlayerDataOnFailure(string collectionName, string userId);
        Task<ClaimStatusResult> UpdatePlayerDataOnSuccess(string collectionName, string userId);



    }
}
