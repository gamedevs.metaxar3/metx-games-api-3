using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IUserService
    {
        Task<Player?> GetUser(string collectionName, string id);
        Task<string> CreateNewUser(string collectionName, string userId, User newUser);
        Task<string> LoginUser(string collectionName, string id, User updatedUser);
    }
}
