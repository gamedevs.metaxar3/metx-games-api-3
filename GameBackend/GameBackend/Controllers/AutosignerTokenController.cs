using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;


namespace GameBackend.Controllers
{
    [ApiController]
    [Route("[controller]/{collectionName}")]
    public class AutosignerTokenController : ControllerBase
    {
        private readonly IAutosignerTokenService _autosignerTokenService;

        public AutosignerTokenController(IAutosignerTokenService autosignerTokenService)
        {
            _autosignerTokenService = autosignerTokenService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewNode([FromRoute] string collectionName, [FromBody] AutosignerToken newNode)
        {
            if (string.IsNullOrEmpty(newNode.Node) || string.IsNullOrEmpty(newNode.BearerToken))
            {
                return BadRequest("Both node and bearerToken fields are required.");
            }

            await _autosignerTokenService.CreateNewNode(collectionName, newNode);
            return Ok("Node created successfully.");
        }

        [HttpPut("{node}")]
        public async Task<IActionResult> UpdateBearerToken(string collectionName, string node, [FromBody] AutosignerToken newNode)
        {
            if (string.IsNullOrEmpty(newNode.Node) || string.IsNullOrEmpty(newNode.BearerToken))
            {
                return BadRequest("Both node and bearerToken fields are required.");
            }

            await _autosignerTokenService.UpdateBearerToken(collectionName, node, newNode); // Corrected method call
            return Ok("Bearer updated successfully.");
        }

        [HttpGet("{node}")]
        public async Task<IActionResult> GetBearerToken(string collectionName, string node)
        {
            var bearer = await _autosignerTokenService.GetBearerToken(collectionName, node);

            if (bearer is null)
            {
                return NotFound();
            }

            return Ok(bearer.BearerToken);
        }

    }
}
