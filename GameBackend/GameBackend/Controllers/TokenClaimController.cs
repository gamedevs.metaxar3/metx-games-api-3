using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameBackend.Controllers
{
    [ApiController]
    [Route("[controller]/{collectionName}")]
    public class TokenClaimController : ControllerBase
    {
        private readonly ITokenClaimService _tokenClaimService;

        public TokenClaimController(ITokenClaimService tokenClaimService)
        {
            _tokenClaimService = tokenClaimService;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetClaimStatus(string collectionName, string userId, string? autosigner_url = null)
        {
            try
            {
                // Call the TokenClaimService to check the claim status
                ClaimStatusResult claimStatus = await _tokenClaimService.CheckClaimStatus(collectionName, userId, autosigner_url);

                if (claimStatus.Success)
                {
                    // Claim status checked and handled successfully
                    return Ok(claimStatus);
                }
                else
                {
                    // Handle the case when the claim status check failed
                    return BadRequest(new { message = claimStatus.Message });
                }
            }
            catch (ArgumentException ex)
            {
                // Handle the case when the player is not found
                return NotFound(new { message = ex.Message });
            }
            catch (System.Exception ex)
            {
                // Handle other exceptions that might occur during claim status check
                // Data will reset

                var updateResult = await _tokenClaimService.UpdatePlayerDataOnFailure(collectionName, userId);
                return StatusCode(500, new { message = "An error occurred while checking the claim status.", data = updateResult.Data });
            }
        }

        [HttpPut("{userId}/Success")]
        public async Task<IActionResult> UpdateTokenClaimSuccess(string collectionName, string userId)
        {
            try
            {
                var tokensClaimUpdateResult = await _tokenClaimService.UpdatePlayerDataOnSuccess(collectionName, userId);

                if (tokensClaimUpdateResult.Success)
                {
                    return StatusCode(200, new { message = "Claim status success. Data updated!", data = tokensClaimUpdateResult.Data });
                }
                else
                {
                    return StatusCode(400, new { success = false, message = tokensClaimUpdateResult.Message });
                }
            }
            catch (ArgumentException ex)
            {
                return StatusCode(404, new { success = false, message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(400, new { success = false, message = ex.Message });
            }
            catch (Exception ex)
            {
                // Log the error
                Console.Error.WriteLine(ex);
                return StatusCode(500, new { success = false, message = "Internal server error" });
            }
        }

        [HttpPut("{userId}/Fail")]
        public async Task<IActionResult> UpdateTokenClaimFail(string collectionName, string userId)
        {
            try
            {
                var tokensClaimUpdateResult = await _tokenClaimService.UpdatePlayerDataOnFailure(collectionName, userId);

                return StatusCode(200, new { message = "Claim status fail. Data updated!", data = tokensClaimUpdateResult.Data });
            }
            catch (ArgumentException ex)
            {
                return StatusCode(404, new { success = false, message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(400, new { success = false, message = ex.Message });
            }
            catch (Exception ex)
            {
                // Log the error
                Console.Error.WriteLine(ex);
                return StatusCode(500, new { success = false, message = "Internal server error" });
            }
        }
    }
}
