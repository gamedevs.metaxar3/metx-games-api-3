using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GameBackend.Controllers
{
    [ApiController]
    [Route("[controller]/{collectionName}")]
    public class TokenRequestController : ControllerBase
    {
        private readonly ITokenRequestService _tokenRequestService;

        public TokenRequestController(ITokenRequestService tokenRequestService)
        {
            _tokenRequestService = tokenRequestService;
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateTokenRequest(string collectionName, string userId, [FromQuery] string? hash = null)
        {
            try
            {
                var tokensReqUpdateResult = await _tokenRequestService.UpdateTokenReq(collectionName, userId, hash);

                if (tokensReqUpdateResult.HashExists)
                {
                    return StatusCode(400, new
                    {
                        success = false,
                        message = "Transaction hash already exists"
                    });
                }

                var viewModel = new
                {
                    success = true,
                    message = "Tokens Requested updated and total score reset",
                    data = new
                    {
                        amount = tokensReqUpdateResult.TokensReq,
                        hash = tokensReqUpdateResult.TxnHash,
                        time = tokensReqUpdateResult.Time
                    }
                };
                return StatusCode(200, viewModel);

            }
            catch (ArgumentException ex)
            {
                return StatusCode(404, new
                {
                    success = false,
                    message = ex.Message
                });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(400, new
                {
                    success = false,
                    message = ex.Message
                });
            }
            catch (Exception ex)
            {
                // Log the error
                Console.Error.WriteLine(ex);
                return StatusCode(500, new
                {
                    success = false,
                    message = "Internal server error"
                });
            }
        }

    }
}
