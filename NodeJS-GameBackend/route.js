const express = require("express");
// const controller = require("./Deprecated/controller.js");
// const readController = require("./controllers/read.controller.js");
// const writeController = require("./controllers/write.controller.js");
const middleware = require("./middleware/auth.middleware.js")
const getAllController = require("./controllers/getAll.controller.js");
const getGameIDController = require("./controllers/getGameID.controller.js");
const getUserDetailsController = require("./controllers/getUserDetails.controller.js");
const checkClaimStatusController = require("./controllers/checkClaimStatus.controller.js");
const dailyScoreController = require("./controllers/dailyScore.controller.js");
const upgradesController = require("./controllers/upgrades.controller.js");
const tokensRequestController = require("./controllers/tokensRequest.controller.js");
const upgradeRequestController = require("./controllers/upgradeRequest.controller.js");
const checkUpgradeStatusController = require("./controllers/checkUpgradeStatus.controller.js");
const highScoreController = require("./controllers/highScore.controller.js");
const route = express.Router();

// Add JWT middleware to verify token before accessing protected routes
const verifyToken = middleware.verifyToken;

route.get("/", getAllController.getAll);

route.get("/:gameID", getGameIDController.getGameID);

route.get("/:gameID/:user", verifyToken, getUserDetailsController.getUserDetails);

route.put("/:gameID/login", middleware.login);

route.put("/:gameID/:user/dailyscore", verifyToken, dailyScoreController.dailyScore);

route.put("/:gameID/:user/tokensreq", verifyToken, tokensRequestController.tokensRequest);

route.put("/:gameID/:user/upgrades", verifyToken, upgradesController.upgrades);

/* route.put("/:gameID/:user/totalscore", verifyToken, controller.totalScore); */

route.get("/:gameID/:user/claimstatus", verifyToken, checkClaimStatusController.checkClaimStatus);

route.put("/:gameID/:user/tokensclaim/success", verifyToken, checkClaimStatusController.updateTokenClaimSuccess);

route.put("/:gameID/:user/tokensclaim/fail", verifyToken, checkClaimStatusController.updateTokenClaimFailbyDuration);

route.put("/:gameID/:user/tokensclaim/fail/duration", verifyToken, checkClaimStatusController.updateTokenClaimFailbyDuration);

route.put("/:gameID/:user/upgradesrequest", verifyToken, upgradeRequestController.upgradesRequest);

route.put("/:gameID/:user/checkUpgradeStatus", verifyToken, checkUpgradeStatusController.checkUpgradeStatus);

route.put("/:gameID/:user/upgrade/success", verifyToken, checkUpgradeStatusController.putUpgradeSuccess);

route.put("/:gameID/:user/upgrade/fail", verifyToken, checkUpgradeStatusController.putUpgradeFail);

route.put("/:gameID/:user/highscore", highScoreController.updateHighScore);

route.get("/:gameID/:user/highscore", highScoreController.getHighScore);

route.put("/:gameID/:user/highscore/tokensreq", tokensRequestController.highScoreTokensRequest);

route.put("/:gameID/:user/highscore/tokensclaim", checkClaimStatusController.updateHighScoreTokenClaim);

route.get("/:gameID/highscore/leaderboard", highScoreController.getHighScoreLeaderboard);

route.get("/:gameID/leaderboard/all", dailyScoreController.getScoreTokenLeaderboard);

module.exports = route;
