`GameBackend`

- use for new tokenized games backend
- c# asp.net
- firestore firebase
- easier to use for NoSQL

`NodeJS-GameBackend`

- used for previous 3 games
- realtime database firebase
- express.js
- for reference only - 5 APIs

https://gitlab.com/gamedevs.metaxar/metx-tokenized-games-backend-cs/-/tree/b9cf8eee4e3ec409ac280dd53a5f26f711c751bb
